public abstract class Building {

    private Integer height;
    private Integer width;
    private Integer floor;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Building(Integer height, Integer width, Integer floor) {
        this.height = height;
        this.width = width;
        this.floor = floor;
    }

    public Integer calcArea(){
        Integer area = getHeight() * getWidth();
        System.out.println("Our area even:" + "\t" + area + " sq.m");
        return area;


    }

}
