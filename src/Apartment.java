public class Apartment extends Building implements Calculation{
    private Integer numberFlat;

    public Integer getNumberFlat() {
        return numberFlat;
    }

    public void setNumberFlat(Integer numberFlat) {
        this.numberFlat = numberFlat;
    }

    public Apartment(Integer numberFlat, Integer getheight, Integer getwidth, Integer getFloor) {
        super(getheight, getwidth, getFloor);
        this.numberFlat = numberFlat;
    }

    //
    @Override
    public Integer calcArea() {
        return super.calcArea();
    }
}
