public class Factory extends Building implements Calculation{

    private Integer rooms;

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public Factory(Integer height, Integer width, Integer floor, Integer rooms) {
        super(height, width, floor);
        this.rooms = rooms;
    }

    @Override
    public Integer calcArea(){
        return super.calcArea();
    }
}
