public class House extends Building implements Calculation, CalcFP {

    private Double sizeFPlaceW;
    private Double sizeFPlaceH;
    private Double sizeFPlaceD;

    public Double getSizeFPlaceW() {
        return sizeFPlaceW;
    }

    public void setSizeFPlaceW(Double sizeFPlaceW) {
        this.sizeFPlaceW = sizeFPlaceW;
    }

    public Double getSizeFPlaceH() {
        return sizeFPlaceH;
    }

    public void setSizeFPlaceH(Double sizeFPlaceH) {
        this.sizeFPlaceH = sizeFPlaceH;
    }

    public Double getSizeFPlaceD() {
        return sizeFPlaceD;
    }

    public void setSizeFPlaceD(Double sizeFPlaceD) {
        this.sizeFPlaceD = sizeFPlaceD;
    }

    public House(Integer height, Integer width, Integer floor, Double sizeFPlaceW, Double sizeFPlaceH, Double sizeFPlaceD) {
        super(height, width, floor);
        this.sizeFPlaceW = sizeFPlaceW;
        this.sizeFPlaceH = sizeFPlaceH;
        this.sizeFPlaceD = sizeFPlaceD;
    }

    public Double squareFPlace(Double sizeFPlaceD, Double sizeFPlaceH, Double sizeFPlaceW){
        Double square = getSizeFPlaceD() * getSizeFPlaceH() * getSizeFPlaceW();
        System.out.println("Result:" +"\t" + square);
        return square;
    }


    @Override
    public Integer calcArea() {
        return super.calcArea();
    }


    @Override
    public Double squareFPlace() {
        return getSizeFPlaceD() * getSizeFPlaceH() * getSizeFPlaceW();
    }
}
