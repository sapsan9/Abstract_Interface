public class ShoppingCentre extends Building {

    private Integer numBoutique;

    public Integer getNumBoutique() {
        return numBoutique;
    }

    public void setNumBoutique(Integer numBoutique) {
        this.numBoutique = numBoutique;
    }

    public ShoppingCentre(Integer height, Integer width, Integer floor, Integer numBoutique) {
        super(height, width, floor);
        this.numBoutique = numBoutique;
    }
}
