public class Main {
    public static void main(String[] args) {

        Apartment apartment = new Apartment(55, 100, 150,50 );

        apartment.calcArea();

        Factory factory = new Factory(50, 250, 5, 45 );

        factory.calcArea();

        ShoppingCentre shoppingCentre = new ShoppingCentre(75, 400, 7, 75);

        shoppingCentre.calcArea();

        House house = new House(10, 6, 1, 1.0, 1.0, 0.5);

        house.calcArea();
        house.squareFPlace(1.0, 1.0, 0.5);


    }
}